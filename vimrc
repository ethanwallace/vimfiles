" COLOUR

" sets colourscheme
"colorscheme molokai

" enables syntax highlighting
syntax on

" TABS/FORMATTING

" sets number of spaces in each tab
set tabstop=8

" sets number of spaces in a tab when editing
set softtabstop=4

" respect current indent-level when making a new line
set autoindent

" replaces tab character with spaces
"set expandtab

" only allow 80 characters per line
set tw=80

" ensure files are UTF-8
set encoding=utf-8

" BUFFER BEHAVIOURS

" sets buffer to open at the bottom
set splitbelow

" sets buffer to open at the right
set splitright

" UI CONFIG

" sets line number
set number

" use relative line numbers
set relativenumber

" activates filetype detection and language-specific indentation
filetype indent on

" stops vim from always redrawing the screen
set lazyredraw

" allows vim to autocomplete commands with :foo<TAB>
set wildmenu

" highlights matching parenthesis
set showmatch

" always displays the statusline
set laststatus=2

" with lightline.vim, no need for redundant mode label
set noshowmode

" FIND/SEARCH OPTIONS

" searches characters as they are entered
set incsearch

" highlights matches
set hlsearch

" MISC OPTIONS

" displays confirmation dialogue when closing an unsaved file
set confirm

" increase the undo limit
set history=1000

" allow backspace over indentation, line-breaks, insertion start
set backspace=indent,eol,start

" fixes some stuff
set nocompatible

" REMAPS

" rebind caps lock to escape when inside vim
au VimEnter * silent! !xmodmap -e 'clear Lock' -e 'keycode 0x42 = Escape'
au VimLeave * silent! !xmodmap -e 'clear Lock' -e 'keycode 0x42 = Caps_Lock'

" rebind split navigation to be more intuitive
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
